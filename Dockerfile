FROM python:3

COPY . /code
WORKDIR /code

RUN python3 -m pip install .

ENTRYPOINT ["python3", "-m", "trellocli"]
