# TrelloCLI

Create Trello cards from the command line.

## Installation

### Using pip

The project can be installed using pip, as follows:

```bash
python3 -m pip install git+https://gitlab.com/inakimalerba/trellocli
```

### Using Debian package

Every pipeline in the `main` branch generates a package that can be downloaded and installed
on your system.

1. Download the [latest artifacts](https://gitlab.com/inakimalerba/trellocli/-/jobs/artifacts/main/download?job=build-debian).

1. Extract the artifacts and install the package named `trellocli_1.0.deb`.

```bash
unzip artifacts.zip
apt-get install artifacts/trellocli_1.0_all.deb
```

### Using Docker image

The project provides a Docker image ready to use for TrelloCLI.

```
docker run --rm -it -v /path/to/trellocli_config:/root/trellocli registry.gitlab.com/inakimalerba/trellocli:latest
```

## Getting started

TrelloCLI requires a `key` and `token` to authenticate against Trello.
These values can be retrieved using the following instructions, and need to be
stored in TrelloCLI config file.

### Config file

By default, the config file is located in `~/trellocli`, but this location can be overriden
on each CLI call.

```yaml
---
key: "your key here"
token: "your token here"
```

### Getting the Key and Token

1. Go to the [App Key](https://trello.com/app-key) page and copy the `Developer API Key`.

1. Manually generate a token by clicking at the `Token` hyperlink.

1. Store both `key` and `token`  values in the config file.


## Creating cards

Cards can be created by CLI arguments, one by one, or using a CSV file as input.

### Single card

To create a single card, the following arguments are expected:

```plain
positional arguments:
  idList                Trello list ID
  name                  New card name

optional arguments:
  --card-params CARD_PARAMS [CARD_PARAMS ...]
                        Extra attributes to set on the card. key=value according to available params in the api
```

By using `--card-params` it's possible to add any of the parameters accepted
by the [Create Card API].

The following example adds the optional `desc` and `pos` arguments.

```bash
trellocli single myIdList 'Name of the card' --card-params pos=bottom desc='description of the card'
```

### Multiple cards

To create multiple cards in a batch, it's possible to define a CSV file containing
all the desired cards.

The CSV file must contain the following columns: `idList` and `name`.
Any other extra column matching the parameters available in the [Create Card API] can be added .

The following example adds the optional `desc` and `pos` argument.

```csv
idList,name,desc,pos
myIdList,Title of the first card,Text for the first card,top
myIdList,Title of the second card,Text for the second card,top
myIdList,Title of the third card,Text for the third card,bottom
```

### Retrieve idList

To create a card it's necessary to define in which list we want it to be created.
This value is not exposed in the Trello interface but can be retrived with
the following steps:

1. Add `.json` ending to your board URL to get the JSON data for your board.

1. Under the `lists` key, find the list you want to put the card in and copy the
   `id` value. This is your `idList`.

[Create Card API]: https://developer.atlassian.com/cloud/trello/rest/api-group-cards/#api-group-cards
