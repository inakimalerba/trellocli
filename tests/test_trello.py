"""Tests for trellocli/trello module."""
import unittest

import requests
import responses

from trellocli import trello
from trellocli import utils


class TestTrello(unittest.TestCase):
    """Test Trello class."""

    def setUp(self):
        """Prepare tests."""
        self.trello_instance = trello.Trello('key', 'token')

    def test_cls_attrs(self):
        """Test defined class attributes."""
        self.assertEqual('https://api.trello.com', trello.Trello.API_URL)
        self.assertEqual('/1/cards', trello.Trello.ENDPOINT_CARDS)

    def test_init(self):
        """Test __init__ function."""
        self.assertEqual('key', self.trello_instance.key)
        self.assertEqual('token', self.trello_instance.token)

    @responses.activate
    def test_post(self):
        """Test post function."""
        responses.add(responses.POST, 'https://api.trello.com/foo')
        response = self.trello_instance.post('/foo', {'foo': 'bar'})

        self.assertEqual(
            'https://api.trello.com/foo?foo=bar&key=key&token=token',
            response.request.url
        )
        self.assertEqual(
            'application/json',
            response.request.headers['Accept']
        )

    @responses.activate
    def test_post_error(self):
        """Test post function checks for errors in the response."""
        responses.add(responses.POST, 'https://api.trello.com/foo', status=401)

        self.assertRaises(
            requests.exceptions.HTTPError,
            self.trello_instance.post, '/foo', {'foo': 'bar'}
        )

    @responses.activate
    def test_create_card(self):
        """Test create_card function."""
        responses.add(responses.POST, 'https://api.trello.com/1/cards')
        self.trello_instance.create_card(
            'abcd1234', {'name': 'foo', 'desc': 'bar'}
        )

        self.assertEqual(
            'https://api.trello.com/1/cards?'
            'idList=abcd1234&name=foo&desc=bar&key=key&token=token',
            responses.calls[0].request.url
        )

    def test_from_config(self):
        """Test from_config class method."""
        cfg = utils.Config('/foo/bar')

        # Config has missing values, should fail.
        self.assertRaises(
            Exception,
            trello.Trello.from_config, cfg
        )

        cfg.content = {'key': 'key', 'token': 'token'}

        trello_instance = trello.Trello.from_config(cfg)

        self.assertEqual('key', trello_instance.key)
        self.assertEqual('token', trello_instance.token)
