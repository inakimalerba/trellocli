"""Tests for trellocli/init module."""
import pathlib
import tempfile
import unittest
from unittest import mock

import trellocli
from trellocli import utils


class TestMain(unittest.TestCase):
    """Test Main class."""

    def setUp(self):
        """Prepare tests."""
        self.config_path = tempfile.NamedTemporaryFile()

        pathlib.Path(self.config_path.name).write_text(
            '---\nkey: foo\ntoken: bar'
        )
        self.config_instance = utils.Config(self.config_path.name)
        self.config_instance.load()

    @mock.patch('trellocli.create_card')
    @mock.patch('trellocli.utils.Config')
    def test_create_single_card(self, config_mock, create_card_mock):
        """
        Test single card creation CLI entrypoint.

        Make sure the key=value arguements are correctly split.
        """
        cfg_mock = mock.Mock()
        config_mock.return_value = cfg_mock

        args = [
            '--config-path', self.config_path.name,
            'single',
            'someidlist',
            'cardname',
            '--card-params', 'param1=value1', 'param2=value with spaces'
        ]
        trellocli.main(args)

        config_mock.assert_called_with(self.config_path.name)
        create_card_mock.assert_called_with(
            cfg_mock,
            'someidlist',
            {'name': 'cardname', 'param1': 'value1', 'param2': 'value with spaces'}
        )

    @mock.patch('trellocli.trello.Trello')
    def test_create_card(self, trello_mock):
        """Test create_card function."""
        trellocli.create_card('config', 'list_id', 'params')
        trello_mock.assert_has_calls([
            mock.call.from_config('config'),
            mock.call.from_config().create_card('list_id', 'params')
        ])

    @mock.patch('trellocli.create_card')
    @mock.patch('trellocli.utils.Config')
    def test_create_multiple_cards(self, config_mock, create_card_mock):
        """Test multiple cards creation CLI entrypoint."""
        cfg_mock = mock.Mock()
        config_mock.return_value = cfg_mock

        csv_file = tempfile.NamedTemporaryFile()

        pathlib.Path(csv_file.name).write_text(
            'listId,name,foo,bar\n'
            'list1,name1,foo1,bar1\n'
            'list2,name2,foo2,bar2\n'
        )

        args = [
            '--config-path', self.config_path.name,
            'multiple',
            csv_file.name
        ]
        trellocli.main(args)

        config_mock.assert_called_with(self.config_path.name)
        create_card_mock.assert_has_calls([
            mock.call(cfg_mock, 'list1', {'name': 'name1', 'foo': 'foo1', 'bar': 'bar1'}),
            mock.call(cfg_mock, 'list2', {'name': 'name2', 'foo': 'foo2', 'bar': 'bar2'}),
        ])
