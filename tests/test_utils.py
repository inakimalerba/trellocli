"""Tests for trellocli/utils module."""
import pathlib
import tempfile
import unittest

from trellocli import utils


class TestConfig(unittest.TestCase):
    """Test Config class."""

    def setUp(self):
        """Prepare tests."""
        self.config_path = tempfile.NamedTemporaryFile()
        self.config_instance = utils.Config(self.config_path.name)

    def test_init(self):
        """Test __init__ function."""
        self.assertEqual(self.config_path.name, self.config_instance.path)

    def test_load(self):
        """Test load function."""
        pathlib.Path(self.config_path.name).write_text(
            '---\nkey: foo\ntoken: bar'
        )
        self.config_instance.load()

        self.assertEqual(
            {'key': 'foo', 'token': 'bar'},
            self.config_instance.content
        )

    def test_get(self):
        """Test get function."""
        pathlib.Path(self.config_path.name).write_text(
            '---\nkey: foo\ntoken: bar'
        )
        self.config_instance.load()
        self.assertEqual('foo', self.config_instance.get('key'))
        self.assertEqual('bar', self.config_instance.get('token'))


class TestCSVToJSON(unittest.TestCase):
    """Test csv_to_json method."""

    def test_convert(self):
        """Test converting csv to json."""
        csv_file = tempfile.NamedTemporaryFile()
        pathlib.Path(csv_file.name).write_text(
            'a,b,c,d\n1,2,3,4\n5,6,7,8'
        )

        self.assertEqual(
            [
                {'a': '1', 'b': '2', 'c': '3', 'd': '4'},
                {'a': '5', 'b': '6', 'c': '7', 'd': '8'}
            ], utils.csv_to_json(csv_file.name)
        )
