"""Init file."""
import argparse

from trellocli import trello
from trellocli import utils


def create_card(config, list_id, params):
    """Create a single card."""
    trello_instance = trello.Trello.from_config(config)
    trello_instance.create_card(list_id, params)


def parse_args(argv=None):
    """Parse CLI arguments."""
    parser = argparse.ArgumentParser(
        description='Create Trello Cards from CLI.')

    parser.add_argument('--config-path', default='~/.trellocli',
                        help='Path to trellocli configuration')

    subparsers = parser.add_subparsers(dest='action')

    # Single card creation parser
    parser_single = subparsers.add_parser(
        'single', help='Create a single card from CLI arguments'
    )
    parser_single.add_argument('idList', help='Trello list ID')
    parser_single.add_argument('name', help='New card name')
    parser_single.add_argument('--card-params', nargs='+',
                               help=('Extra attributes to set on the card. '
                                     'key=value according to available params in the api'))
    # Multiple card creation parser
    parser_single = subparsers.add_parser(
        'multiple', help='Create multiple cards from a CSV file'
    )
    parser_single.add_argument('path', help='Path to CSV file containing the cards content.')

    return parser.parse_args(argv)


def main(argv=None):
    """Create Trello Cards from CLI."""
    args = parse_args(argv)

    config = utils.Config(args.config_path)
    config.load()

    if args.action == 'single':
        # Parse k=v arguments from card_params
        card_params = dict([
            param.split('=') for param in args.card_params
        ])

        create_card(
            config,
            args.idList,
            {
                'name': args.name,
                **card_params
            }
        )
    elif args.action == 'multiple':
        cards = utils.csv_to_json(args.path)
        for card in cards:
            create_card(
                config,
                card.pop('listId'),
                {
                    'name': card.pop('name'),
                    **card
                }
            )
