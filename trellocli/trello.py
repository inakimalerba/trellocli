"""Trello API wrapper."""
import requests

DEFAULT_HEADERS = {
   'Accept': 'application/json'
}


class Trello:
    """Trello API wrapper."""

    API_URL = 'https://api.trello.com'
    ENDPOINT_CARDS = '/1/cards'

    def __init__(self, key, token):
        """Initialize client."""
        self.key = key
        self.token = token

    @classmethod
    def from_config(cls, config):
        """Create Trello instance from config."""
        return cls(config.get('key'), config.get('token'))

    def post(self, endpoint, params):
        """Perform POST request."""
        params.update({
            'key': self.key,
            'token': self.token,
        })
        response = requests.post(self.API_URL + endpoint, params=params, headers=DEFAULT_HEADERS)
        response.raise_for_status()
        return response

    def create_card(self, list_id, content):
        """
        Create a new card on the given list.

        Content accepts any parameter allowed for creating a card according
        to the documentation.
        """
        params = {'idList': list_id, **content}
        self.post(self.ENDPOINT_CARDS, params)
