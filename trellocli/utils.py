"""Utilities."""
import csv
import pathlib

import yaml


class Config:
    """Configuration loader."""

    def __init__(self, path):
        """Initialize."""
        self.path = path
        self.content = None

    def load(self):
        """Load config from file."""
        self.content = yaml.safe_load(
            pathlib.Path(self.path).read_text(encoding='utf8')
        )

    def get(self, key):
        """Get value for config key."""
        return self.content[key]


def csv_to_json(path):
    """Read a CSV file and convert it to json."""
    with pathlib.Path(path).open(encoding='utf8') as file:
        return list(csv.DictReader(file))
